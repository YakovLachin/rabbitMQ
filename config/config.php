<?php

return array(
    'host'       => 'localhost',
    'port'       => 5672,
    'user'       => 'guest',
    'password'   => 'guest',
    'secret_key' => 'secret_key'
);
